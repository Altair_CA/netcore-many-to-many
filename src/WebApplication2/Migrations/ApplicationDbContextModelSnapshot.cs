﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApplication2.Data;

namespace WebApplication2.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApplication2.Model.Customer", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("fullName");

                    b.HasKey("id");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("WebApplication2.Model.CustomerSupplier", b =>
                {
                    b.Property<int>("customerId");

                    b.Property<int>("suplierId");

                    b.HasKey("customerId", "suplierId");

                    b.HasIndex("customerId");

                    b.HasIndex("suplierId");

                    b.ToTable("CustomerSupplier");
                });

            modelBuilder.Entity("WebApplication2.Model.Supplier", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("name");

                    b.HasKey("id");

                    b.ToTable("supplier");
                });

            modelBuilder.Entity("WebApplication2.Model.CustomerSupplier", b =>
                {
                    b.HasOne("WebApplication2.Model.Customer", "customer")
                        .WithMany("supplier")
                        .HasForeignKey("customerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApplication2.Model.Supplier", "suplier")
                        .WithMany("customer")
                        .HasForeignKey("suplierId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
