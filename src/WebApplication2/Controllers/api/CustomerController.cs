﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.Model;
using WebApplication2.Data;
using Microsoft.EntityFrameworkCore;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication2.Controllers.api
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext _db;

        public CustomerController(ApplicationDbContext context)
        {
            _db = context;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult get()
        {
            return Json(_db.customer.Include(x=> x.supplier).ToList());
        }
        [HttpPost]
        public IActionResult post([FromBody]Customer _customer)
        {
            Customer dbCustomer = new Customer();

            dbCustomer.fullName = _customer.fullName;

            _db.customer.Add(dbCustomer);
            _db.SaveChanges();

            return Json(dbCustomer);
        }
        [Route("addsup")]
        [HttpPost("{id}/{supplierId}")]
        public IActionResult addSuplier(int id, int supplierId)
        {
            Customer dbCustomer = _db.customer.Where(x => x.id == id).Include(x=> x.supplier).First();
            Supplier dbSupplier = _db.supplier.Where(x => x.id == supplierId).First();

            dbCustomer.supplier.Add(new Model.CustomerSupplier { suplier = dbSupplier });

            _db.SaveChanges();

            return Json(dbCustomer);
        }
    }
}
