﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Model;

namespace WebApplication2.Data
{
    public class ApplicationDbContext:DbContext
    {
        public DbSet<Supplier> supplier { get; set; }

        public DbSet<Customer> customer { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CustomerSupplier>()
                .HasKey(t => new { t.customerId, t.suplierId });

            builder.Entity<CustomerSupplier>()
                .HasOne(p => p.customer)
                .WithMany(x => x.supplier)
                .HasForeignKey(y => y.customerId);

            builder.Entity<CustomerSupplier>()
                .HasOne(p => p.suplier)
                .WithMany(x => x.customer)
                .HasForeignKey(y => y.suplierId);

        }
    }
}
